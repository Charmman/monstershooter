﻿using UnityEngine;
using System.Collections;

public class spawnEnemy : MonoBehaviour {

	public GameObject monster ;
	public bool isRandom;
	public float range;
	public float instantiateRate;
	public int lastIndex = 0;
	public Transform player1;
	public Transform player2;
	public EnemyHealth enemy;
	private bool spawnDead;
	private EnemyHealth enemyHP;
	private FollowThing followScript;
	private int select;

	void Start () {

		InvokeRepeating ("InstantiateFloor", 0, instantiateRate);
	}
	

	void Update()
	{

	}

	
	
	
	void InstantiateFloor(){





		if (isRandom) {

			Vector3 newPos = new Vector3 (this.transform.position.x , this.transform.position.y, this.transform.position.z + Random.Range (-range, range));
			
			GameObject clone = Instantiate (monster, newPos, this.transform.rotation) as GameObject;
			followScript = clone.GetComponent<FollowThing>();
			enemyHP = clone.GetComponent<EnemyHealth>();
			enemyHP.dead = false;

			
			select = Random.Range (1,3);
			
			if(select == 1)
			{
				followScript.core = player1;
			}else{
				followScript.core = player2;
			}


		}

		
	}


}

