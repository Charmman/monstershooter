﻿using UnityEngine;
using System.Collections;

public class Player2Move : MonoBehaviour {
	
	public float rotationSpeed = 450f;
	
	
	private Rigidbody rb;
	private Vector3 horizontalDirection;
	private Vector3 verticalDirection;
	private Quaternion targetRotation;
	
	
	public float hSpeed;
	public float vSpeed;
	
	
	void Start () 
	{
		
		rb = GetComponent<Rigidbody> ();
		
	}
	
	
	void Update () 
	{
		float moveHorizontal = Input.GetAxis ("Player2H");
		float moveVertical = Input.GetAxis ("Player2V");
		
		
		
		horizontalDirection = new Vector3 (moveHorizontal, 0, 0);
		verticalDirection = new Vector3 (0, 0, moveVertical);
		
		/*
		float inputX = Input.GetAxisRaw ("Look");
		float inputY = Input.GetAxisRaw ("Look2");
		if (inputX != 0.0f || inputY != 0.0f) 
		{
			//float newAngle = Mathf.Atan2 (inputX, -inputY) * Mathf.Rad2Deg;
			//transform.eulerAngles = new Vector3 (transform.eulerAngles.x, newAngle, transform.eulerAngles.z);
		}
		*/
		
		/*Vector3 lookPoint = new Vector3 (targetSphere.position.x, 0, targetSphere.position.y);
		
		transform.LookAt (lookPoint);*/
		
		
		
	}
	
	void FixedUpdate()
	{
		
		rb.AddRelativeForce (horizontalDirection * hSpeed);
		rb.AddRelativeForce (verticalDirection * vSpeed);
	}
}
