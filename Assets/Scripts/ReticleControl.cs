﻿using UnityEngine;
using System.Collections;

public class ReticleControl : MonoBehaviour {

	float distanceFromCam = 10;
	public Transform lookTarget;
	public Transform player;
	private Vector3 offset;

	// Use this for initialization
	void Start () 
	{

		player = transform.root;
		offset = transform.position - transform.root.position;

		transform.SetParent (null);


	}
	
	// Update is called once per frame
	void Update () 
	{
	
		//float moveHorizontal = Input.GetAxis ("Look");
		//float moveVertical = Input.GetAxis ("Look2");

		//transform.Rotate (-moveHorizontal, moveVertical,0);

	
		//Vector3 moveVector = new Vector3 (moveHorizontal, -moveVertical, 0);

		//transform.position += moveVector;

	}


	void LateUpdate()
	{
		transform.position = player.position + offset;
		//Quaternion rot = new Quaternion (0, player.transform.rotation.y, 0, 0);
		//transform.rotation = rot;
		transform.LookAt(lookTarget);
	}
}
