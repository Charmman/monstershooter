﻿using UnityEngine;
using System.Collections;

public class FollowPlayer2 : MonoBehaviour {
	
	Transform player2;
	public float RotSpeed,moveSpeed;
	public EnemyHealth enemy;

	void Start () 
	{
		player2 = GameObject.FindGameObjectWithTag ("Player2").transform;
		enemy = GetComponent <EnemyHealth>();
	}
	
	
	void Update () 
	{
		if (enemy.dead == false)
		{
		transform.rotation = Quaternion.Slerp (transform.rotation, Quaternion.LookRotation(player2.position - transform.position), RotSpeed * Time.deltaTime);
		
		transform.position += transform.forward * moveSpeed * Time.deltaTime;
		}
	}

}
