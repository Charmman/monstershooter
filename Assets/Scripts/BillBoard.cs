﻿using UnityEngine;
using System.Collections;

public class BillBoard : MonoBehaviour {
	
	public Camera cam;
	public Camera cam2;
	
	void Awake ()
	{
	
	}
	
	void Update () 
	{
		LookAtCamera();
		LookAtCamera2 ();
	}
	
	void LookAtCamera ()
	{
		transform.LookAt( transform.position + cam.transform.rotation *
		                 Vector3.forward, cam.transform.rotation * Vector3.up);
	}

	void LookAtCamera2 ()
	{
		transform.LookAt( transform.position + cam2.transform.rotation *
		                 Vector3.forward, cam2.transform.rotation * Vector3.up);
	}
}