﻿using UnityEngine;
using System.Collections;

public class FollowPlayer : MonoBehaviour {

	Transform player;
	public float RotSpeed,moveSpeed;
	public EnemyHealth enemy;

	void Start () 
	{
		player = GameObject.FindGameObjectWithTag ("Player").transform;
		enemy = GetComponent <EnemyHealth>();
	}
	

	void Update () 
	{
		if (enemy.dead == false)
		{
		transform.rotation = Quaternion.Slerp (transform.rotation, Quaternion.LookRotation(player.position - transform.position), RotSpeed * Time.deltaTime);

		transform.position += transform.forward * moveSpeed * Time.deltaTime;
		}
	}
}
