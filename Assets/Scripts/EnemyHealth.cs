﻿using UnityEngine;
using System.Collections;

public class EnemyHealth : MonoBehaviour, IDamageable
{

    public int startingHealth = 3;

	private Animator anim;

    private int currentHealth;

	public bool dead;
	public bool spawnDead;
    void Start()
    {

		anim = GetComponent< Animator>();
        currentHealth = startingHealth;

    }



    public void Damage(int damage)
    {
        currentHealth -= damage;
        if (currentHealth <= 0)
        {
			dead =true;
			anim.SetTrigger("Death");
			Invoke ("Defeated",2.5f);
        }else{
			dead = false;
		}
		if(gameObject.tag == "spawner" && dead == true)
		{
			spawnDead = true;
		}else{
			spawnDead = false;
		}

    }

    void Defeated()
    {
	
		Destroy (gameObject);

    }



}
