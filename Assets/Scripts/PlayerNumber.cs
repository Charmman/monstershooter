using UnityEngine;
using System.Collections;

public class PlayerNumber : MonoBehaviour {

	public int playerNumber = 1;

	public string player1Shoot = "Fire1";
	public string player1Reload = "Reload";
	public string player2Shoot = "Player2Shoot";
	public string player2Reload = "Player2Reload";
	public string player1V = "Vertical";
	public string player1H = "Horizontal";
	public string player2H = "Player2H";
	public string player2V = "Player2V";
	public string playerYMouse = "Mouse X";
	public string playerXMouse = "Mouse Y";
	public string player1Look = "Look";
	public string player2Look = "Player2Look";
	public string player1Look2 = "Look2";
	public string player2Look2 = "Player2Look2";
	public string player1Sprint = "Sprint";
	public string player2Sprint = "Sprint2";




	public GameObject player1Cam;
	public GameObject player2Cam;

	private Camera myCam;
	public Shooter player1Shooter;
	public Shooter player2Shooter;
	public bool useMouse;

	private SimpleSmoothMouseLook mouseLook;
	private MovementScript movementScript;

	private Animator anim;

	// Use this for initialization
	void Awake () 
	{
		myCam = GetComponentInChildren<Camera>();
		movementScript = GetComponent<MovementScript>();
		mouseLook = GetComponent<SimpleSmoothMouseLook>();
		anim = GetComponent<Animator>();
	}

	void Start()
	{
		if (playerNumber == 1)
		{
			player1Shooter.shootAxisName = player1Shoot;
			player1Shooter.reloadAxisName = player1Reload;
			player1Cam.SetActive(true);
			player2Cam.SetActive(false);
			movementScript.moveH = player1H;
			movementScript.moveV = player1V;
			mouseLook.lookHGamePad = player1Look;
			mouseLook.lookVGamePad = player1Look2;
			movementScript.sprinting = player1Sprint;
			movementScript.reload = player1Reload;
		}

		if (playerNumber == 2)
		{
			player2Shooter.shootAxisName = player2Shoot;
			player2Shooter.reloadAxisName = player2Reload;
			player2Cam.SetActive(true);
			player1Cam.SetActive(false);
			movementScript.moveH = player2H;
			movementScript.moveV = player2V;
			mouseLook.lookHGamePad = player2Look;
			mouseLook.lookVGamePad = player2Look2;
			movementScript.sprinting = player2Sprint;
			movementScript.reload = player2Reload;
		}

		if (playerNumber == 3)
		{
			player1Shooter.shootAxisName = player1Shoot;
			player1Shooter.reloadAxisName = player1Reload;
			player1Cam.SetActive(true);
			player2Cam.SetActive(false);
			movementScript.moveH = "Horizontal";
			movementScript.moveV = "Vertical";
			mouseLook.lookHGamePad = playerYMouse;
			mouseLook.lookVGamePad = playerXMouse;
			movementScript.sprinting = player1Sprint;
			movementScript.reload = player1Reload;
		}

	}


}
