﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class Shooter : MonoBehaviour {

	public GameObject bullet;
	public Transform spawnPoint;
	public Transform lookAt;
	public MovementScript move;
	public GameObject flare;
	public AnimationClip reloadAnimation;

	private float nextFire;
	public float fireRate;
	public int damage = 1;

	public Text bulletText;
	public Text extraBulletText;

	public int bulletCount = 50;

	public int extraBullets = 150;

	//public ParticleSystem shootParticles;
	public GameObject hitParticles;
	//public GameObject shootFlare;
	public float weaponRange = 10000000000;
	private WaitForSeconds shotLength = new WaitForSeconds(.07f);

	private Camera cam;

	public string shootAxisName = "Fire1";
	public string reloadAxisName ="Reload";

	public Text txt;


	private LineRenderer lineRenderer;
	public Animator anim;
	public PlayerHealth health;
	public bool isReloading;
	public bool canReload;
	public RandAudio audio;
	public AudioClip re;
	private AudioSource aud;

	void Awake()
	{
		cam = GetComponentInParent<Camera> ();
		lineRenderer = GetComponent<LineRenderer> ();
		//anim = GetComponentInParent<Animator>();
		aud = GetComponent<AudioSource>();
	}

	void Update () 
	{
		RaycastHit hit;
		SayReload();
		anim.SetBool("Reloading", isReloading);
		if (move.isWalking == true && isReloading)
		{
			anim.SetBool ("WalkingReload",true);
		}else{
			anim.SetBool("WalkingReload",false);
		}

		if (Input.GetButtonDown (reloadAxisName) && extraBullets > 0 && bulletCount != 50 && move.isSprinting == false) 
		{
			aud.PlayOneShot (re, 0.7f);
			canReload = true;
			Invoke ("Reload", 2.85f);
			isReloading = true;


		}


		
	
		
		if (Input.GetButton(shootAxisName) && Time.time > nextFire && bulletCount >0 && isReloading == false && health.isDead == false && move.isSprinting == false && move.reloads == false)
		{
			anim.SetTrigger ("isShooting");
			audio.PlayRandomized ();
			Vector3 lineEnd;
			nextFire = Time.time + fireRate;
			StartCoroutine(ShotEffect());
			Vector3 rayOrigin = cam.ViewportToWorldPoint (new Vector3(.5f, .5f, 0));
			//Instantiate(shootFlare, spawnPoint.position,Quaternion.identity);


			if (Physics.Raycast(rayOrigin, cam.transform.forward, out hit, weaponRange))
			{
				IDamageable dmgScript = hit.collider.GetComponent<IDamageable>();
				if (dmgScript != null){
				//Debug.DrawRay(rayOrigin, cam.transform.forward * weaponRange, Color.green);
					dmgScript.Damage(damage);
				Instantiate(hitParticles, hit.point, Quaternion.FromToRotation(Vector3.up, hit.normal));

				}
				lineEnd = hit.point;
			}
			else {
				lineEnd = cam.ViewportToWorldPoint (new Vector3(.5f, .5f, 100));
			}
			bulletCount--;

			lineRenderer.SetPosition(0, transform.position);
			lineRenderer.SetPosition(1, lineEnd);
			bulletText.text = bulletCount.ToString("f0");

		}

		}



	public void Reload()
	{
		isReloading = false;
		move.reloads = false;

		bulletText.color = Color.white;
		if (bulletCount < 50 && health.isDead == false) 
		{
				int bulletsToReload;
				bulletsToReload = 50 - bulletCount;
				if(extraBullets >= bulletsToReload) {
					extraBullets -= bulletsToReload; 
					bulletCount += bulletsToReload;
				}
				else {
					bulletCount += extraBullets;
					extraBullets = 0;
				}
		}

		bulletText.text = bulletCount.ToString("f0");
		extraBulletText.text = extraBullets.ToString ("f0");

	}

    public void AddBullets()
    {
        extraBullets += 500;
		extraBulletText.text = extraBullets.ToString ("f0");
        print("Hit");
    }

	void SayReload()
	{
		if (bulletCount <= 15)
		{
			bulletText.color = Color.red;
			txt.color = Color.red;
			txt.text = "RELOAD!!";
			txt.horizontalOverflow = HorizontalWrapMode.Wrap;
			print (txt);

		}
		else{
			txt.text = "";
		}
	}
	private IEnumerator ShotEffect()
	{
		//source.Play();
		//shootParticles.Play();
		//shootFlare.SetActive(true);
		flare.gameObject.SetActive (true);
		hitParticles.SetActive(true);
		lineRenderer.enabled = true;
		yield return shotLength;
		lineRenderer.enabled = false;
		flare.gameObject.SetActive (false);
		//shootFlare.SetActive(false);
		hitParticles.SetActive(false);
	}

}


