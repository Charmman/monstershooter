﻿using UnityEngine;
using System.Collections;

public class RandAudio: MonoBehaviour {
	
	public AudioClip[] jumpSounds; 
	
	private AudioSource source;
	private float vollowRange = .5f;
	private float volHighRange = 1.0f;
	private float pitchLow = .9f;
	private float pitchHigh = 1.1f;
	
	void Awake ()
	{
		source = GetComponent <AudioSource> ();
	}
	
	public void PlayRandomized ()
	{
		float volScaler = Random.Range (vollowRange, volHighRange);
		float pitch = Random.Range (pitchLow, pitchHigh);
		source.pitch = pitch;
		source.PlayOneShot (jumpSounds[Random.Range (0, jumpSounds.Length)], source.volume * volScaler);
	}
}