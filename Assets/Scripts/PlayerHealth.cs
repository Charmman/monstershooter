﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class PlayerHealth : MonoBehaviour {

	public int startingHealth = 100;                            // The amount of health the player starts the game with.
	public int currentHealth;  
	public Slider healthSlider;  
	public Image damageImage;  
	public float flashSpeed = 5f;  
	public Color flashColour = new Color(1f, 0f, 0f, 0.1f); 

	int health =100;
	public Text healthText;
	Animator anim;
	public bool isDead;
	bool damaged;  
	private float fadeDuration = 1;
	public Color textDamageColor = Color.red;
	public Color textStandardColor = Color.white;
	float currentFadeTime;

	void Awake()
	{
		currentHealth = startingHealth;
	}

	void Start () 
	{
		anim = GetComponent<Animator>();
	}
	

	void Update () 
	{
		if(health <=0)
		{
			health = 0;

		}



		if (health == 0)
		{
			anim.SetBool ("Death",true);
			isDead = true;
		}

		if (damaged)
		{
			damaged = false;
			StartCoroutine(FadeColor(1));
		}



		healthText.text = "HP: " + health;

	}

	private IEnumerator FadeColor(float time)
	{

	
		float elapsedTime = 0;
		healthText.color = textDamageColor;
		
		while (elapsedTime < time)
		{
			healthText.color = Color.Lerp(textDamageColor, textStandardColor, (elapsedTime / time));
			elapsedTime += Time.deltaTime;
			yield return new WaitForEndOfFrame();
		}

	}

	void OnCollisionEnter(Collision other)
	{
		if(other.gameObject.tag == "enemy")
		{
			LoseHealth();
		}
	}

	public void LoseHealth()
	{
		health -= 5;
		damaged = true;
		currentFadeTime = 0;
	}
	
	void Death ()
	{
		// Set the death flag so this function won't be called again.
		isDead = true;
		

		
		// Tell the animator that the player is dead.
		anim.SetTrigger ("Die");

	}       
}
	
