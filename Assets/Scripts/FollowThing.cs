﻿using UnityEngine;
using System.Collections;

public class FollowThing : MonoBehaviour {

    public Transform core;
    NavMeshAgent agent;
	private Animator anim;
	public EnemyHealth enemy;

	void Start ()
    {
		enemy = GetComponent <EnemyHealth>();
        agent = GetComponent<NavMeshAgent>();
		anim = GetComponent<Animator>();

	}
	
	
	void Update ()
    {
		if (enemy.dead == false)
		{
        agent.SetDestination(core.position);
		anim.SetTrigger ("Running");
		}
	}
}
