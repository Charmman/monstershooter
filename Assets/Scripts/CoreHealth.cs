﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CoreHealth : MonoBehaviour {

	public int health = 100;
	public Text healthText;
	float speed;
	public bool finished;
		
	void Start () 
	{
	}

	void Update () 
	{
		if (health <= 0) 
		{
			Destroy(gameObject);
			finished = true;
		}
		healthText.text = health.ToString ("f0");
	}
	
	void OnCollisionEnter(Collision other)
	{
		if (other.gameObject.tag == "enemy") 
		{
			health--;
		}
	}
}
