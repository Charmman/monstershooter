﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ShootProtoType : MonoBehaviour {
	
	public GameObject bullet;
	public Transform spawnPoint;
	
	private float nextFire;
	public float fireRate;
	
	public Text bulletText;
	public Text extraBulletText;
	
	public int bulletCount = 50;
	
	private int extraBullets = 150;
	
	
	public string shootAxisName = "Fire1";
	public string reloadAxisName ="Reload";
	
	public Text txt;
	
	
	void Start()
	{
	}
	
	
	void Update () 
	{
		if (Input.GetButton (shootAxisName) && Time.time > nextFire && bulletCount > 0) 
		{
			nextFire = Time.time + fireRate;
			Instantiate(bullet,spawnPoint.transform.position,transform.rotation);
			bulletCount--;
			
		}
		bulletText.text = bulletCount.ToString("f0");
		extraBulletText.text = extraBullets.ToString ("f0");
		Reload ();
	}
	
	
	void Reload()
	{
		if (bulletCount <= 15)
		{
			txt.text = "RELOAD!!";
			txt.horizontalOverflow = HorizontalWrapMode.Wrap;
			print (txt);
		}
		else{
			txt.text = "";
		}
		if (bulletCount < 50) 
		{
			if (Input.GetButton (reloadAxisName) && extraBullets > 0) {
				int bulletsToReload;
				bulletsToReload = 50 - bulletCount;
				
				if(extraBullets >= bulletsToReload) {
					extraBullets -= bulletsToReload; 
					bulletCount += bulletsToReload;
				}
				else {
					bulletCount += extraBullets;
					extraBullets = 0;
				}
			}
		}
	}
	
	public void AddBullets()
	{
		extraBullets += 100;
		print("Hit");
	}
	
}