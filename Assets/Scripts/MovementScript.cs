﻿using UnityEngine;
using System.Collections;

public class MovementScript : MonoBehaviour {

	public float rotationSpeed = 450f;

	public Shooter shoot;

	private Rigidbody rb;
	private Vector3 horizontalDirection;
	private Vector3 verticalDirection;


	private Quaternion targetRotation;

	public string moveH;
	public string moveV;
	public string sprinting;
	public string reload;


	public float hSpeed;
	public float vSpeed;
	public float sprintSpeed;
	public AudioClip runStep;


	private Animator anim;
	private float moveHorizontal;
	private float moveVertical;
	private AudioSource aud;

	private PlayerHealth health;

	public bool isSprinting;
	public bool isWalking;
	public bool reloads;
	public AudioClip breathing;


	void Start () 
	{
		health = GetComponent<PlayerHealth>();
		rb = GetComponent<Rigidbody> ();
		anim = GetComponent<Animator>();
		aud = GetComponent<AudioSource>();
	}
	
	
	void Update () 
	{
		if (health.isDead == false)
		{
		moveHorizontal = Input.GetAxis (moveH);
		moveVertical = Input.GetAxis (moveV);



		horizontalDirection = new Vector3 (moveHorizontal, 0, 0);
		verticalDirection = new Vector3 (0, 0, moveVertical);
	

		if (Mathf.Abs(moveHorizontal) > Mathf.Epsilon || Mathf.Abs(moveVertical) > Mathf.Epsilon)
			{
				if(Input.GetButton(sprinting))
				{
					if(!aud.isPlaying){
						aud.PlayOneShot(breathing, 0.7f); 
					aud.PlayOneShot(runStep, 0.7f);}
					
					isSprinting = true;
					isWalking = false;
				}
				else
				{
					aud.Stop();
					isSprinting = false;
					isWalking = true;
				}

			}






		Walk();
		
		}
	}

	void FixedUpdate()
	{

		rb.AddRelativeForce (horizontalDirection * hSpeed);
		rb.AddRelativeForce (verticalDirection * vSpeed);

		if (isSprinting)
		{
		rb.AddRelativeForce (horizontalDirection * sprintSpeed);
		rb.AddRelativeForce (verticalDirection * sprintSpeed);
		}
	}

	public void Walk()
	{
		if (health.isDead == false)
		{

		


			if (Mathf.Abs(moveHorizontal) > Mathf.Epsilon || Mathf.Abs(moveVertical) > Mathf.Epsilon)
			{
				if(isSprinting)
				{
					anim.SetBool ("Sprint",true);
					anim.SetBool("Walking",false);
				}
				else
				{
					anim.SetBool ("Sprint",false);
					anim.SetBool("Walking",true);
				}

			}

			else
				
			{
				anim.SetBool ("Walking", false);
				anim.SetBool ("Sprint", false);
			}
		}

		

	

	}

	void GetReload()
	{
		shoot.Reload();
		reloads = false;
	}


}
