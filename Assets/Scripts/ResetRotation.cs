﻿using UnityEngine;
using System.Collections;

public class ResetRotation : MonoBehaviour {

	public float pos;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void LateUpdate () 
	{
		Quaternion lockedRotation = new Quaternion (0, transform.rotation.y, 0, transform.rotation.w);
		Vector3 lockedPosition = new Vector3 (transform.position.x, pos, transform.position.z);
		transform.position = lockedPosition;
		transform.rotation = lockedRotation;
	}
}
