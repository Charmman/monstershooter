﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Timer : MonoBehaviour {


	public float myTimer;
	private Text timerText;


	void Start () 
	{
		timerText = GetComponent<Text>();
	}
	

	void Update () 
	{
		myTimer -=  Time.deltaTime;
		timerText.text = myTimer.ToString("f0");
	}
}
